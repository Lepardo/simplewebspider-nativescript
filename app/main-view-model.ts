import { Observable } from '@nativescript/core';
import * as Crawler from "nativescript-worker-loader!./workers/crawler/crawler";
import { Settings } from './shared/Settings';
const HIDDEN = "hidden";
const SHOWN = "";
const crawler = new Crawler();

export class HelloWorldModel extends Observable {
    private _buttonText: string;
    private _message: string;
    private _settings: Settings;
    private _inputVisibility: string;
    private _runVisibility: string;

    constructor() {
        super();

        // Initialize default values.
        this._buttonText = "START";
        this._settings = new Settings("", 102400, 4);
        this._inputVisibility = SHOWN;
        this._runVisibility = HIDDEN;
        this._message = "Fresh";
        crawler.onmessage = (msg) => {
            this.message = msg.data;
        }
        crawler.onerror = (err) => {
            const msg = `An unhandled error occurred in worker: ${err.filename}, line: ${err.lineno} : ${err.message}`
            console.log(msg);
            this.message = msg;
        }
    }

    public get buttonText(): string {
        return this._buttonText;
    }
    public set buttonText(v: string) {
        if (this._buttonText !== v) {
            this._buttonText = v;
            this.notifyPropertyChange('buttonText', v);
        }
    }


    public get settings(): Settings {
        return this._settings;
    }
    public set settings(value: Settings) {
        this._settings = value;
        this.notifyPropertyChange('settings', value);
    }

    get inputVisibility(): string {
        return this._inputVisibility;
    }
    set inputVisibility(value: string) {
        if (this._inputVisibility !== value) {
            this._inputVisibility = value;
            this.notifyPropertyChange('inputVisibility', value);
        }
    }

    get runVisibility(): string {
        return this._runVisibility;
    }
    set runVisibility(value: string) {
        if (this._runVisibility !== value) {
            this._runVisibility = value;
            this.notifyPropertyChange('runVisibility', value);
        }
    }

    get message(): string {
        return this._message;
    }
    set message(value: string) {
        if (this._message !== value) {
            this._message = value;
            this.notifyPropertyChange('message', value);
        }
    }

    onTap() {
        if (this.buttonText === "START") {
            this.startCrawler();
        } else {
            this.stopCrawler();
        }
    }

    startCrawler() {
        this.runVisibility = SHOWN;
        this.inputVisibility = HIDDEN;
        this.buttonText = "STOP";
        this.message = "Started...";
        crawler.postMessage({ command: "START", settings: this._settings });
    }

    stopCrawler() {
        this.runVisibility = HIDDEN;
        this.inputVisibility = SHOWN;
        this.buttonText = "START";
        this.message = "Waiting for start";
        crawler.postMessage({ command: "STOP", settings: this._settings });
    }
}

