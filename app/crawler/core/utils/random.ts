function randomFloat(min: number, max: number): number {
  return Math.random() * (max - min) + min;
}

function randomInt(min: number, max: number): number {
  min = Math.ceil(min);
  max = Math.floor(max);

  if (min > max) {
    throw new Error(`Invalid parametes: min is greater than max [min=${min}, max=${max}]`);
  }
  if (min < Number.MIN_SAFE_INTEGER) {
    throw new Error(`Invalid parametes: min is less than min safe integer [min=${min}, max=${max}]`);
  }
  if (max > Number.MAX_SAFE_INTEGER) {
    throw new Error(`Invalid parametes: max is greater than max safe integer [min=${min}, max=${max}]`);
  }

  if (min === max) {
    return min;
  }

  const result = Math.floor(Math.random() * (max - min)) + min;
  return result;
}

export { randomFloat, randomInt };
