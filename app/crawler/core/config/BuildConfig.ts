import { IBuildConfig } from "~/crawler/core/config/IBuildConfig";

export class BuildConfig implements IBuildConfig {
  constructor(that: IBuildConfig) {
    this.name = that.name;
    this.version = that.version;
    this.timestamp = that.timestamp;
    this.sha = that.sha;
  }
  name: string;
  version: string;
  timestamp: string;
  sha: string;
}
