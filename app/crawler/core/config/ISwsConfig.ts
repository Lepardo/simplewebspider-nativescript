import { IDatabaseConfig, IHostThrottlerConfig } from "~/crawler/service/database";
import { IExtractorConfig, IFilterInvalidUrlConfig } from "~/crawler/service/extractor";
import { IQueueConfig, IQueueThrottlerConfig } from "~/crawler/service/queue";
import { IBuildConfig } from "~/crawler/core/config/IBuildConfig";

export interface ISwsConfig {
  readonly build: IBuildConfig;
  readonly database: IDatabaseConfig;
  readonly queue: IQueueConfig;
  readonly queueThrottler: IQueueThrottlerConfig;
  readonly hostThrottler: IHostThrottlerConfig;
  readonly extractor: IExtractorConfig;
  readonly filterInvalidUrl: IFilterInvalidUrlConfig;
}
