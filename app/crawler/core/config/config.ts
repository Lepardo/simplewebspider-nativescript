import { InvalidConfigurationError } from "~/crawler/core/config/InvalidConfigurationError";
import { SwsConfig } from "~/crawler/core/config/SwsConfig";


function initializeConfiguration(): SwsConfig {
  try {
    return new SwsConfig({
      build: {
        name: "simplewebspider-nativescript",
        version: "undefined version",
        timestamp: "undefined timestamp",
        sha: "undefined sha",
      },
      database: {
        size: 10 * 1024 * 1024,
      },
      queue: {
        parallel: 4,
      },
      queueThrottler: {
        perMinute: 10,
        minWait: 100,
        randomWait: 1000,
      },
      hostThrottler: {
        size: 1024 * 1024,
        preload: 128,
        ttl: 60 * 60 * 1000,
      },
      extractor: {
        httpMaxResponseSize: 10 * 1024 * 1024,
        httpTimeout: 30 * 1000,
        urlMaxLength: 1024,
      },
      filterInvalidUrl: {
        allowedSize: 1024 * 1024,
        blockedSize: 1024 * 1024,
      },
    });
  } catch (e) {
    throw new InvalidConfigurationError(`Configuration invalid`, e);
  }
}

const config = initializeConfiguration();

export { config };
