import { DatabaseConfig, HostThrottlerConfig, IDatabaseConfig, IHostThrottlerConfig } from "~/crawler/service/database";
import { ExtractorConfig, FilterInvalidUrlConfig, IExtractorConfig, IFilterInvalidUrlConfig } from "~/crawler/service/extractor";
import { IQueueConfig, IQueueThrottlerConfig, QueueConfig, QueueThrottlerConfig } from "~/crawler/service/queue";
import { BuildConfig } from "~/crawler/core/config/BuildConfig";
import { IBuildConfig } from "~/crawler/core/config/IBuildConfig";
import { ISwsConfig } from "~/crawler/core/config/ISwsConfig";

export class SwsConfig implements ISwsConfig {
  build: IBuildConfig;
  database: IDatabaseConfig;
  queue: IQueueConfig;
  queueThrottler: IQueueThrottlerConfig;
  hostThrottler: IHostThrottlerConfig;
  extractor: IExtractorConfig;
  filterInvalidUrl: IFilterInvalidUrlConfig;

  constructor(that: ISwsConfig) {
    this.build = new BuildConfig(that.build);
    this.database = new DatabaseConfig(that.database);
    this.queue = new QueueConfig(that.queue);
    this.queueThrottler = new QueueThrottlerConfig(that.queueThrottler);
    this.hostThrottler = new HostThrottlerConfig(that.hostThrottler);
    this.extractor = new ExtractorConfig(that.extractor);
    this.filterInvalidUrl = new FilterInvalidUrlConfig(that.filterInvalidUrl);
  }
}
