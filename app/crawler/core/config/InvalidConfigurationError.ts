export class InvalidConfigurationError extends Error {
  private cause: Error | undefined;
  constructor(message: string, e?: Error) {
    super(message);
    this.cause = e;
    this.name = `InvalidConfigurationError`;
  }
}
