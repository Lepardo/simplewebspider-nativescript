import { config } from "~/crawler/core/config";
import { SimpleBootstrapper } from "~/crawler/service/bootstrap";
import { SimplerCrawler, ICrawler } from "~/crawler/service/crawler";
import { HostThrottlerDatabase, LimitedMemoryDatabase } from "~/crawler/service/database";
import { FilterInvalidUrlsExtrator, SimpleExtractor } from "~/crawler/service/extractor";
import { PromiseQueue, QueueThrottlerQueue } from "~/crawler/service/queue";

export function createCrawler(): ICrawler {
    console.log(`Build ${JSON.stringify(config.build)}`);
    console.log(`Configuration ${JSON.stringify(config)}`);

    console.log(`Wire services`);
    const bootstrapper = new SimpleBootstrapper();
    const limitedMemorydatabase = new LimitedMemoryDatabase(config.database);
    const hostThrottlerDatabase = new HostThrottlerDatabase(limitedMemorydatabase, config.hostThrottler);
    const simpleExtractor = new SimpleExtractor(config.extractor);
    const filterInvalidUrlsExtrator = new FilterInvalidUrlsExtrator(simpleExtractor, config.filterInvalidUrl);
    const queue = new PromiseQueue(config.queue);
    const queueThrottlerQueue = new QueueThrottlerQueue(queue, config.queueThrottler);
    const crawler = new SimplerCrawler(hostThrottlerDatabase, bootstrapper, filterInvalidUrlsExtrator, queueThrottlerQueue);

    return crawler;
}
