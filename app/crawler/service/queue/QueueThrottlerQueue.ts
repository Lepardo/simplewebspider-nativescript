import { randomInt } from "~/crawler/core/utils";
import { IQueue } from "~/crawler/service/queue/IQueue";
import { IQueueThrottlerConfig } from "~/crawler/service/queue/IQueueThrottlerConfig";

function sleep(ms: number): Promise<void> {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

const oneMinute = 60_000;

class Throttler {
  readonly perMinute: number;
  readonly minWait: number;
  readonly randomWait: number;
  readonly times: number[];
  readonly staticWait: number;

  async next(): Promise<void> {
    if (this.perMinute <= 0) {
      return;
    }

    const wait = this.getWait();
    console.log(`Waiting [milliseconds=${wait}]`);
    await sleep(wait);
    this.put();
  }

  getWait(): number {
    let wait = Math.floor((this.cleanup() + this.staticWait) / 2);
    wait += randomInt(0, this.randomWait);
    wait = wait < this.minWait ? this.minWait : wait;
    wait = wait > oneMinute ? oneMinute : wait;
    return wait;
  }

  cleanup(): number {
    const oneMinuteAgo = Date.now() - oneMinute;

    while (this.times.length >= this.perMinute) {
      const entry = this.times.shift();
      console.log(`Removed entry [entry=${entry}, size=${this.times.length}]`);
      if (entry === undefined) {
        return 0;
      }
      if (entry >= oneMinuteAgo) {
        return entry - oneMinuteAgo;
      }
    }

    return 0;
  }

  put(): void {
    this.times.push(Date.now());
  }

  constructor(config: IQueueThrottlerConfig) {
    this.perMinute = config.perMinute;
    this.minWait = config.minWait;
    this.randomWait = config.randomWait;
    this.staticWait = Math.ceil(oneMinute / config.perMinute); // ceil ensure at least one
    this.times = [];
  }
}

class QueueThrottlerQueue implements IQueue {
  readonly queue: IQueue;
  readonly throttler: Throttler;

  async add(func: (this: void) => Promise<void>): Promise<void> {
    await this.throttler.next();
    await this.queue.add(func);
  }

  constructor(queue: IQueue, config: IQueueThrottlerConfig) {
    this.queue = queue;
    this.throttler = new Throttler(config);
  }
}

export { QueueThrottlerQueue };
