import pAny from "p-any";
import { v4 as uuid } from "uuid";
import { IQueue } from "~/crawler/service/queue/IQueue";
import { IQueueConfig } from "~/crawler/service/queue/IQueueConfig";

export class PromiseQueue implements IQueue {
  readonly parallel: number;
  readonly queue: Map<string, Promise<void>>;

  async add(func: (this: void) => Promise<void>): Promise<void> {
    const id = uuid();
    console.log(`Enqueue [id=${id}, size=${this.queue.size}]`);
    this.queue.set(id, this.execute(id, func));

    if (this.queue.size >= this.parallel) {
      console.log(`Waiting... [size=${this.queue.size}]`);
      await pAny(this.queue.values());
      console.log(`Continue... [size=${this.queue.size}]`);
    }
  }

  async execute(id: string, func: (this: void) => Promise<void>): Promise<void> {
    console.log(`Starting [id=${id}, size=${this.queue.size}]`);
    try {
      await func();
    } catch (e) {
      console.log(`Execution failed ${id}`, e);
    } finally {
      console.log(`Removing [id=${id}, size=${this.queue.size}]`);
      this.queue.delete(id);
    }
  }

  constructor(config: IQueueConfig) {
    this.queue = new Map();
    this.parallel = config.parallel;
  }
}
