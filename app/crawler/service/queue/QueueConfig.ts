import { IQueueConfig } from "~/crawler/service/queue/IQueueConfig";

export class QueueConfig implements IQueueConfig {
  parallel: number;

  constructor(that: IQueueConfig) {
    this.parallel = that.parallel;
  }
}
