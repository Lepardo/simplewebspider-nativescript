import { IQueueThrottlerConfig } from "~/crawler/service/queue/IQueueThrottlerConfig";

export class QueueThrottlerConfig implements IQueueThrottlerConfig {
  perMinute: number;
  minWait: number;
  randomWait: number;

  constructor(that: IQueueThrottlerConfig) {
    this.perMinute = that.perMinute;
    this.minWait = that.minWait;
    this.randomWait = that.randomWait;
  }
}
