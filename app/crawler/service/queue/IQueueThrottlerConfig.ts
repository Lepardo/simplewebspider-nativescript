export interface IQueueThrottlerConfig {
  readonly randomWait: number;
  readonly perMinute: number;
  readonly minWait: number;
}
