import { IDatabase } from "~/crawler/service/database/IDatabase";
import { IHostThrottlerConfig } from "~/crawler/service/database/IHostThrottlerConfig";

class HostEntry {
  readonly hostname: string;
  usage: number;
  lastAccessEpoch: number;

  touch(): void {
    this.usage++;
    this.lastAccessEpoch = Date.now();
    console.log(`Touched host entry ${JSON.stringify(this)}`);
  }

  constructor(hostname: string) {
    this.hostname = hostname;
    this.lastAccessEpoch = Date.now();
    this.usage = 0;
  }
}

class HostStorage {
  readonly size: number;
  readonly ttl: number;
  hosts: HostEntry[];

  hostEntry(url: URL): HostEntry {
    this.removeOld();

    const hostname = url.hostname;
    if (!hostname) {
      throw new Error(`Invalid URL: no hostname [url=${url}]`);
    }

    let host = this.hosts.find((host) => host.hostname === hostname);
    if (host === undefined) {
      host = new HostEntry(hostname);
    }
    return host;
  }

  touch(url: URL): void {
    const hostEntry = this.hostEntry(url);
    if (hostEntry.usage === 0) {
      this.add(hostEntry);
    }
    hostEntry.touch();
  }

  removeOld(): void {
    const oldest = Date.now() - this.ttl;
    this.hosts = this.hosts.filter((entry) => entry.lastAccessEpoch > oldest);
  }

  add(hostEntry: HostEntry): void {
    this.hosts.push(hostEntry);
    this.hosts.sort((a, b) => b.lastAccessEpoch - a.lastAccessEpoch);
    this.hosts.splice(this.size, this.hosts.length - this.size);
  }

  constructor(config: IHostThrottlerConfig) {
    this.size = config.size;
    this.ttl = config.ttl;
    this.hosts = [];
  }
}

export class HostThrottlerDatabase implements IDatabase {
  readonly preload: number;
  readonly hostStorage: HostStorage;
  readonly database: IDatabase;

  constructor(database: IDatabase, config: IHostThrottlerConfig) {
    this.database = database;
    this.preload = config.preload;
    this.hostStorage = new HostStorage(config);
  }

  async remove(): Promise<URL | undefined> {
    const candidates = await this.candidates();

    candidates.sort((a, b) => this.usageComperator(a, b));
    const item = candidates.shift();
    if (item === undefined) {
      return undefined;
    }

    await this.database.add(...candidates);

    try {
      this.hostStorage.touch(item);
    } catch (e) {
      console.log(`Failed to touch host entry [item=${item}]`, e);
      throw new Error(`Failed to touch host entry [item=${item}]`);
    }
    return item;
  }

  async add(...items: URL[]): Promise<this> {
    await this.database.add(...items);
    return this;
  }

  async candidates(): Promise<URL[]> {
    const candidates: URL[] = [];
    while (candidates.length < this.preload) {
      const candidate = await this.database.remove();
      if (candidate === undefined) {
        break;
      }
      candidates.push(candidate);
    }
    return candidates;
  }

  usageComperator(a: URL, b: URL): number {
    const aEntry = this.hostStorage.hostEntry(a);
    const bEntry = this.hostStorage.hostEntry(b);
    return aEntry.usage - bEntry.usage;
  }

  async size(): Promise<number> {
    return await this.database.size();
  }
}
