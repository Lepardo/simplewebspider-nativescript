import { IDatabaseConfig } from "~/crawler/service/database/IDatabaseConfig";

export class DatabaseConfig implements IDatabaseConfig {
  size: number;

  constructor(that: IDatabaseConfig) {
    this.size = that.size;
  }
}
