import { randomInt } from "~/crawler/core/utils";
import { IDatabase } from "~/crawler/service/database/IDatabase";
import { IDatabaseConfig } from "~/crawler/service/database/IDatabaseConfig";

class LimitedMemoryDatabase implements IDatabase {
  readonly maxSize: number;
  entries: Set<string>;

  constructor(databaseConfig: IDatabaseConfig) {
    this.maxSize = databaseConfig.size;
    this.entries = new Set();
  }

  async size(): Promise<number> {
    return this.entries.size;
  }

  async remove(): Promise<URL | undefined> {
    const index = randomInt(0, this.entries.size);
    console.log(`Removing element [index=${index}, length=${this.entries.size}]`);
    let count = 0;
    let item: URL | undefined = undefined;
    this.entries.forEach((value) => {
      if (count++ === index) {
        item = new URL(value);
      }
    });
    if (item) {
      this.entries.delete(item.toString());
    }
    return item;
  }

  async add(...items: URL[]): Promise<this> {
    console.log(`Add entry: ${items}`);
    items.forEach((item) => this.entries.add(item.toString()));
    while (this.entries.size > this.maxSize) {
      this.entries.forEach((value, _value, set) => {
        if (set.size > this.maxSize) {
          console.log(`Deleting entry [size=${set.size}, value=${value}]`);
          set.delete(value);
        }
      });
    }
    return this;
  }
}

export { LimitedMemoryDatabase };
