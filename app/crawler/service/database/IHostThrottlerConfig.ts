export interface IHostThrottlerConfig {
  readonly preload: number;
  readonly size: number;
  readonly ttl: number;
}
