import { IHostThrottlerConfig } from "~/crawler/service/database/IHostThrottlerConfig";

export class HostThrottlerConfig implements IHostThrottlerConfig {
  preload: number;
  size: number;
  ttl: number;

  constructor(that: IHostThrottlerConfig) {
    this.size = that.size;
    this.preload = that.preload;
    this.ttl = that.ttl;
  }
}
