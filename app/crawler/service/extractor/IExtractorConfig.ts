export interface IExtractorConfig {
  readonly httpMaxResponseSize: number;
  readonly httpTimeout: number;
  readonly urlMaxLength: number;
}
