import { IFilterInvalidUrlConfig } from "~/crawler/service/extractor/IFilterInvalidUrlConfig";

export class FilterInvalidUrlConfig implements IFilterInvalidUrlConfig {
  allowedSize: number;
  blockedSize: number;

  constructor(that: IFilterInvalidUrlConfig) {
    this.allowedSize = that.allowedSize;
    this.blockedSize = that.blockedSize;
  }
}
