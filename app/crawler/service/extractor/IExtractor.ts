export interface IExtractor {
  extract(URL: URL): Promise<Set<URL>>;
}
