//import { LookupAddress, promises as dns } from "dns";
import { filter } from "p-iteration";
//import isPrivateIp from "private-ip";
import { IExtractor } from "~/crawler/service/extractor/IExtractor";
import { IFilterInvalidUrlConfig } from "~/crawler/service/extractor/IFilterInvalidUrlConfig";

/*
function reunshift<T>(array: T[], entry: T): T[] {
  array = array.filter((item) => item !== entry);
  array.unshift(entry);
  return array;
}

function unshift<T>(array: T[], entry: T, limit: number): T[] {
  array.unshift(entry);
  if (limit > 0) {
    array.splice(limit, array.length - limit);
  }
  return array;
}
*/

export class FilterInvalidUrlsExtrator implements IExtractor {
  readonly extractor: IExtractor;
  blockedHostnames: string[];
  allowedHostnames: string[];
  readonly blockedSize: number;
  readonly allowedSize: number;

  async extract(url: URL): Promise<Set<URL>> {
    let urls = [...(await this.extractor.extract(url))];
    urls = urls.filter((item) => this.isHttp(item));
    urls = await filter(urls, async (item) => await this.isAllowed(item));
    return new Set(urls);
  }

  isHttp(url: URL): boolean {
    return url.toString().startsWith(`http://`) || url.toString().startsWith(`https://`);
  }

  async isAllowed(url: URL): Promise<boolean> {
    return true;
    // TODO Reimplemnt this
    /*
    const hostname = url.hostname;

    const allowed = this.allowedHostnames.find((a) => a === hostname);
    if (allowed) {
      console.log(`Already allowed [hostname=${hostname}]`);
      this.allowedHostnames = reunshift(this.allowedHostnames, hostname);
      return true;
    }

    const blocked = this.blockedHostnames.find((b) => b === hostname);
    if (blocked) {
      console.log(`Already blocked [hostname=${hostname}]`);
      this.blockedHostnames = reunshift(this.blockedHostnames, hostname);
      return false;
    }
    new android.net.DnsResolver();
    let lookup: LookupAddress;
    try {
      lookup = await dns.lookup(hostname);
    } catch (e) {
      console.log(`Failed to lookup dns. Add to block list [hostename=${hostname}, error=${e?.message}]`);
      console.log(`Failed to lookup dns. Add to block list [hostename=${hostname}]`, e);
      unshift(this.blockedHostnames, hostname, this.blockedSize);
      return false;
    }

    const address = lookup.address;
    const privateIp = isPrivateIp(address);
    if (privateIp) {
      console.log(`Blocking [hostname=${hostname}]`);
      unshift(this.blockedHostnames, hostname, this.blockedSize);
      return false;
    }

    console.log(`Allowing [hostname=${hostname}]`);
    unshift(this.allowedHostnames, hostname, this.allowedSize);
    return true;
    */
  }

  constructor(extractor: IExtractor, config: IFilterInvalidUrlConfig) {
    this.extractor = extractor;
    this.blockedSize = config.allowedSize;
    this.allowedSize = config.blockedSize;
    this.blockedHostnames = [];
    this.allowedHostnames = [];
  }
}
