import { IExtractorConfig } from "~/crawler/service/extractor/IExtractorConfig";

export class ExtractorConfig implements IExtractorConfig {
  httpMaxResponseSize: number;
  httpTimeout: number;
  urlMaxLength: number;

  constructor(that: IExtractorConfig) {
    this.httpMaxResponseSize = that.httpMaxResponseSize;
    this.httpTimeout = that.httpTimeout;
    this.urlMaxLength = that.urlMaxLength;
  }
}
