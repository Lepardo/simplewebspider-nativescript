import { IDatabase } from "~/crawler/service/database";
import { BootstrapError } from "~/crawler/service/bootstrap/BootstrapError";
import { IBootstrapper } from "~/crawler/service/bootstrap/IBootstrapper";

export class SimpleBootstrapper implements IBootstrapper {
  async run(database: IDatabase): Promise<void> {
    try {
      const content = ["https://ar.wikipedia.org/wiki/Special:Random",
        "https://ceb.wikipedia.org/wiki/Special:Random",
        "https://de.wikipedia.org/wiki/Special:Random",
        "https://de.wikipedia.org/wiki/Special:Random",
        "https://en.wikipedia.org/wiki/Special:Random",
        "https://fr.wikipedia.org/wiki/Special:Random",
        "https://es.wikipedia.org/wiki/Special:Random",
        "https://it.wikipedia.org/wiki/Special:Random",
        "https://ja.wikipedia.org/wiki/Special:Random",
        "https://nl.wikipedia.org/wiki/Special:Random",
        "https://pl.wikipedia.org/wiki/Special:Random",
        "https://pt.wikipedia.org/wiki/Special:Random",
        "https://ru.wikipedia.org/wiki/Special:Random",
        "https://sv.wikipedia.org/wiki/Special:Random",
        "https://uk.wikipedia.org/wiki/Special:Random",
        "https://vi.wikipedia.org/wiki/Special:Random",
        "https://war.wikipedia.org/wiki/Special:Random",
        "https://zh.wikipedia.org/wiki/Special:Random",
        "https://ar.wikinews.org/wiki/Special:Random",
        "https://ceb.wikinews.org/wiki/Special:Random",
        "https://de.wikinews.org/wiki/Special:Random",
        "https://de.wikinews.org/wiki/Special:Random",
        "https://en.wikinews.org/wiki/Special:Random",
        "https://fr.wikinews.org/wiki/Special:Random",
        "https://es.wikinews.org/wiki/Special:Random",
        "https://it.wikinews.org/wiki/Special:Random",
        "https://ja.wikinews.org/wiki/Special:Random",
        "https://nl.wikinews.org/wiki/Special:Random",
        "https://pl.wikinews.org/wiki/Special:Random",
        "https://pt.wikinews.org/wiki/Special:Random",
        "https://ru.wikinews.org/wiki/Special:Random",
        "https://sv.wikinews.org/wiki/Special:Random",
        "https://uk.wikinews.org/wiki/Special:Random",
        "https://vi.wikinews.org/wiki/Special:Random",
        "https://war.wikinews.org/wiki/Special:Random",
        "https://zh.wikinews.org/wiki/Special:Random",
        "https://ar.wikiquote.org/wiki/Special:Random",
        "https://ceb.wikiquote.org/wiki/Special:Random",
        "https://de.wikiquote.org/wiki/Special:Random",
        "https://de.wikiquote.org/wiki/Special:Random",
        "https://en.wikiquote.org/wiki/Special:Random",
        "https://fr.wikiquote.org/wiki/Special:Random",
        "https://es.wikiquote.org/wiki/Special:Random",
        "https://it.wikiquote.org/wiki/Special:Random",
        "https://ja.wikiquote.org/wiki/Special:Random",
        "https://nl.wikiquote.org/wiki/Special:Random",
        "https://pl.wikiquote.org/wiki/Special:Random",
        "https://pt.wikiquote.org/wiki/Special:Random",
        "https://ru.wikiquote.org/wiki/Special:Random",
        "https://sv.wikiquote.org/wiki/Special:Random",
        "https://uk.wikiquote.org/wiki/Special:Random",
        "https://vi.wikiquote.org/wiki/Special:Random",
        "https://war.wikiquote.org/wiki/Special:Random",
        "https://zh.wikiquote.org/wiki/Special:Random",
        "https://ar.wikivoyage.org/wiki/Special:Random",
        "https://ceb.wikivoyage.org/wiki/Special:Random",
        "https://de.wikivoyage.org/wiki/Special:Random",
        "https://de.wikivoyage.org/wiki/Special:Random",
        "https://en.wikivoyage.org/wiki/Special:Random",
        "https://fr.wikivoyage.org/wiki/Special:Random",
        "https://es.wikivoyage.org/wiki/Special:Random",
        "https://it.wikivoyage.org/wiki/Special:Random",
        "https://ja.wikivoyage.org/wiki/Special:Random",
        "https://nl.wikivoyage.org/wiki/Special:Random",
        "https://pl.wikivoyage.org/wiki/Special:Random",
        "https://pt.wikivoyage.org/wiki/Special:Random",
        "https://ru.wikivoyage.org/wiki/Special:Random",
        "https://sv.wikivoyage.org/wiki/Special:Random",
        "https://uk.wikivoyage.org/wiki/Special:Random",
        "https://vi.wikivoyage.org/wiki/Special:Random",
        "https://war.wikivoyage.org/wiki/Special:Random",
        "https://zh.wikivoyage.org/wiki/Special:Random",
        "https://news.google.com/topstories?hl=en-US&gl=US&ceid=US:en",
        "https://news.google.com/topstories?hl=de&gl=DE&ceid=DE:de",
        "https://commons.wikimedia.org/wiki/Special:Random/File",
        "https://meta.wikimedia.org/wiki/Special:Random",
        "https://species.wikimedia.org/wiki/Special:Random",
        "https://wikimania.wikimedia.org/",
        "https://www.wikidata.org/wiki/Special:Random",
        "https://www.wikipedia.org/",
        "https://blogsofnote.blogspot.com/",
        "https://digg.com/",
        "https://linkarena.com/",
        "https://wordpress.org/showcase/",
        "https://www.reddit.com/",
        "https://www.rss-nachrichten.de/",
        "https://www.rss-verzeichnis.de/",
        "https://www.uroulette.com/"];
      content
        //.split(/\r?\n/) // Split by line break
        .map((line) => line && line.trim()) // Trim whitespaces
        .filter((line) => !!line) // Ignore empty lines
        .filter((line) => {
          // Skip comments
          const result = !line.startsWith(`#`) && !line.startsWith(`//`);
          if (!result) {
            console.log(`Skipping line: outcommented [line=${line}]`);
          }
          return result;
        }) //
        .filter((line) => {
          // Only allow http(s)
          const result = line.startsWith(`http://`) || line.startsWith(`https://`);
          if (!result) {
            console.log(`Skipping line: not http or https [line=${line}]`);
          }
          return result;
        }) //
        .filter((line) => {
          // Only allow valid URLs
          try {
            new URL(line);
            return true;
          } catch (e) {
            console.log(`Skipping line: no valid URL [line=${line}, error=${e?.message}]`);
            console.log(`Skipping line: no valid URL [line=${line}]`, e);
            return false;
          }
        }) //
        .map((line) => new URL(line)) //
        .forEach((url) => {
          console.log(`Using URL [url=${url.toString()}]`);
          database.add(url);
        });

      const databaseSize = await database.size();
      if (databaseSize === 0) {
        throw new Error(`Bootstrapping failed: effectivly no URLs found`);
      }
    } catch (e) {
      throw new BootstrapError(`Bootstrapping failed`, e);
    }
  }

  constructor() {
  }
}
