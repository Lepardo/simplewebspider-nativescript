import { IDatabase } from "~/crawler/service/database";
export interface IBootstrapper {
  run(database: IDatabase): Promise<void>;
}
