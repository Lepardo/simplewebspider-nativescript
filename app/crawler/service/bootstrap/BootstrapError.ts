
export class BootstrapError extends Error {
  cause: Error | undefined;
  constructor(message: string, cause?: Error) {
    super(message);
    this.cause = cause;
  }
}
