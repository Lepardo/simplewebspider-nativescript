export interface ICrawler {
  run(): Promise<void>;
  stop(): void;
  onError: (e: any) => void;
  onUrl: (url: string) => void;
}
