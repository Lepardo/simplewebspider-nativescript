import { IBootstrapper } from "~/crawler/service/bootstrap";
import { IDatabase } from "~/crawler/service/database";
import { IExtractor } from "~/crawler/service/extractor";
import { IQueue } from "~/crawler/service/queue";
import { ICrawler } from "~/crawler/service/crawler/ICrawler";

export class SimplerCrawler implements ICrawler {
  database: IDatabase;
  bootstrapper: IBootstrapper;
  extractor: IExtractor;
  queue: IQueue;
  isRunning: boolean;
  onError: (e: any) => void;
  onUrl: (url: string) => void;


  stop(): void {
    console.log(`Trigger crawler stop...`);
    this.isRunning = false;
  }

  async run(): Promise<void> {
    if (this.isRunning) {
      console.log(`Already running. Skipped`)
      return;
    }

    console.log(`Starting crawler...`);
    this.isRunning = true;
    try {
      while (this.isRunning) {
        const url = await this.database.remove();
        if (url === undefined) {
          console.log(`Bootstrapping...`);
          await this.bootstrapper.run(this.database);
          continue;
        }

        await this.queue.add(() => this.execute(url));
      }
    } catch (e) {
      console.error(`Main runner failed`, e);
      this.onError(`Main runner failed: ${e.filename}, line: ${e.lineno} : ${e.message} - ${JSON.stringify(e)}`);
    } finally {
      this.isRunning = false;
      console.log(`Crawler stopped...`)
    }
  }

  async execute(url: URL): Promise<void> {
    try {
      try {
        console.log(`Processing ${url}`);
        if (this.onUrl) {
          this.onUrl(url.toString());
        }
        const urls = await this.extractor.extract(url);
        this.database.add(...urls);
      } catch (e) {
        console.log(`Failed to process [url=${url}]`, e);
        throw new Error(`Failed to process [url=${url}, e=${JSON.stringify(e)}]`);
      }
    } catch (e) {
      console.log(`Failed to execute`, e);
      if (this.onError) {
        this.onError(`Failed to execute: ${e.filename}, line: ${e.lineno} : ${e.message} - ${JSON.stringify(e)}`);
      }
    }
  }

  constructor(database: IDatabase, bootstrapper: IBootstrapper, extractor: IExtractor, queue: IQueue) {
    this.database = database;
    this.bootstrapper = bootstrapper;
    this.extractor = extractor;
    this.queue = queue;
    this.isRunning = false;
  }
}
