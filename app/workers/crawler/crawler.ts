import "globals";
//import { createCrawler } from "~/crawler/server";
//import { Settings } from "~/shared/Settings";

const context: Worker = self as any;
context.onmessage = msg => {
    const command = msg.data.command;
    //lastSettings = msg.data.settings;
    console.log(`Received ${JSON.stringify(msg.data)}`);
    if (command === "START") {
        setTimeout(() => {
            (<any>global).postMessage(`Starting...`)
            try {
                const server = require("~/crawler/server");
                const crawler = server.createCrawler();
                crawler.onError = e => (<any>global).postMessage(e)
                crawler.onUrl = url => (<any>global).postMessage(url)

                //crawler.run();
            } catch (e) {
                console.log("Failed to start crawler", e);
                (<any>global).postMessage(`Failed to start crawler: ${e.filename}, line: ${e.lineno} : ${e.message} - ${JSON.stringify(e)}`);
            }
        }, 250);
    } else if (command === "STOP") {
        setTimeout(() => {
            try {
                //crawler.stop();
                (<any>global).postMessage(`Stopping...`);
            } catch (e) {
                console.log("Failed to start crawler", e);
                (<any>global).postMessage(`Failed to stop crawler: ${e.filename}, line: ${e.lineno} : ${e.message} - ${JSON.stringify(e)}`);
            }
        }, 0);
    }
};

