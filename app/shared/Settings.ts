export class Settings {
    public url: string;
    public queueSize: number;
    public requestLimit: number;

    constructor(url: string, queueSize: number, requestLimit: number) {
        this.url = url;
        this.queueSize = queueSize;
        this.requestLimit = requestLimit;
    }
}