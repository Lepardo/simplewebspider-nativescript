# simplewebspider-nativescrip

Simple Web Spider using [NativeScript](https://nativescript.org/).

## Setup

1. Clone project
2. Install NativeScript CLI: `npm install -g nativescript@latest`
3. Preview the app: `tns preview`
4. Build:
   * Android:
     1. Install latest SDK:
        1. Install the Android Studio <https://developer.android.com/studio>
        2. Start the Android Studio to install the SDK
        3. Open `Configuration`
        4. Verify the SDK installion
        5. Get the SDK path
        6. Set `ANDROID_HOME`
     2. Build `tns build android --compileSdk 29`
   * iOS: 
     1. Not tested
     2. Build `tns build ios`

## Known issues

### dns resolving is not possible

```txt
ERROR in ./crawler/service/extractor/FilterInvalidUrlExtrator.ts
Module not found: Error: Can't resolve 'dns' in 'D:\data\vscode\simplewebspider-nativescript\app\crawler\service\extractor'
 @ ./crawler/service/extractor/FilterInvalidUrlExtrator.ts 3:0-38 55:27-30
 @ . sync (?<!\bApp_Resources\b.*)(?<!\.\/\btests\b\/.*?)\.(xml|css|js|(?<!\.d\.)ts|(?<!\b_[\w-]*\.)scss)$
 @ ./app.ts

ERROR in crawler/service/extractor/FilterInvalidUrlExtrator.ts:66:21
TS2339: Property 'DnsResolver' does not exist on type 'typeof net'.
    64 |       return false;
    65 |     }
  > 66 |     new android.net.DnsResolver();
       |                     ^^^^^^^^^^^
    67 |     let lookup: LookupAddress;
    68 |     try {
    69 |       lookup = await dns.lookup(hostname);
```

See also <https://stackoverflow.com/questions/66843486/how-to-use-android-net-dnsresolver-in-nativescript>

### Import of crawler is not working

```js
import { createCrawler } from "~/crawler/server";
const server = require("~/crawler/server");
```

Causes unknown error: `Cannot read property 'env' of undefined`

Most likely `htmlparser2` is not working and tries to read `process.xyz` information
